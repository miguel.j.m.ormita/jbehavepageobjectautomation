package com.sample.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class MainCategoryTab extends BasePage{
	
	@FindBy(xpath="//a[text()='Women']")
	WebElement womenTab;
	
	@FindBy(xpath="//ul[contains(@class, 'submenu')]//a[@title='Dresses']")
	WebElement dressesTab;
	
	@FindBy(xpath="//ul[contains(@class, 'submenu')]//a[@title='T-shirts']")
	WebElement tshirtsTab;
	
	private static final String CAT_BAR_XPATH_LOC = "//ul[contains(@class, 'menu-content')]";
	@FindBy(xpath=CAT_BAR_XPATH_LOC)
	WebElement categoryTabBar;
	By categoryTabBarLoc = By.xpath(CAT_BAR_XPATH_LOC);
	
	private static final String PAGE_NAME = "Main Category Tab Bar";
	
	public MainCategoryTab(WebDriver lDriver) throws Exception {
		
		super(lDriver);
		PageFactory.initElements(lDriver, this);
		this.validatePageDisplayed(PAGE_NAME, this.categoryTabBarLoc);
	}
	



}
