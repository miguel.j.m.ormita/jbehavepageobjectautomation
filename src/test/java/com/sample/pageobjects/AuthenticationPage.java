package com.sample.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AuthenticationPage extends BasePage{
	
	private static final String PAGE_NAME = "Authentication";
	
	@FindBy(xpath = "//input[@id='email']")
	WebElement userNameField;
	
	@FindBy(xpath = "//input[@id='passwd']")
	WebElement passwordField;
	
	@FindBy(xpath = "//button[@id='SubmitLogin']")
	WebElement signInButton;
	
	private static final String AUTH_HDRTEXT_XPATHLOC = "//h1[text()='Authentication']";
	@FindBy(xpath = AUTH_HDRTEXT_XPATHLOC)
	WebElement authentictionHdrText;
	By authHdrTextLoc = By.xpath(AUTH_HDRTEXT_XPATHLOC);
	
	
	public AuthenticationPage(WebDriver lDriver) throws Exception {
		
		super(lDriver);
		PageFactory.initElements(lDriver, this);
		this.validatePageDisplayed(PAGE_NAME, authHdrTextLoc);
		
	}

	public void enterUsername(String userName) {
		
		this.userNameField.sendKeys(userName);	
	}
	
	public void enterPassword(String password) {
		
		this.passwordField.sendKeys(password);
	}
	
	public MyAccountPage clickSignIn() throws Exception {
		
		this.signInButton.click();
		
		return new MyAccountPage(this.driver);	
	}
	

}
