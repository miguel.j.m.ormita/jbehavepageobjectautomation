package com.sample.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class MyAccountPage extends BasePage{
	
	private static final String PAGE_NAME = "My Account";
	
	private static final String MY_ACCOUNT_HDRTEXT_XPATHLOC = "//h1[text()='My account']";
	@FindBy(xpath = MY_ACCOUNT_HDRTEXT_XPATHLOC)
	WebElement myAccountHeaderText;
	By myAccountHdrTextLoc = By.xpath(MY_ACCOUNT_HDRTEXT_XPATHLOC);
	
	
	public MyAccountPage(WebDriver lDriver) throws Exception {
		
		super(lDriver);
		PageFactory.initElements(lDriver, this);
		this.validatePageDisplayed(PAGE_NAME, myAccountHdrTextLoc);
		
	}
	

}
