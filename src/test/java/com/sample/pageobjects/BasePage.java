package com.sample.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BasePage {
	
	public String PAGE_NAME;
	
	private static final int TIMEOUT = 15;
	
	private static final int POLLING = 100;
	
	WebDriver driver;
	
	WebDriverWait wait;
	
	BasePage(WebDriver lDRiver) throws Exception{
		this.driver = lDRiver;
		wait = new WebDriverWait(lDRiver, TIMEOUT, POLLING);
	}
	
	protected void waitForElementToAppear(By locator) {
		wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
	}
	

	protected void waitForElementToDisappear(By locator) {
		wait.until(ExpectedConditions.invisibilityOfElementLocated(locator));
	}
	
	
	public void validatePageDisplayed(String pageName, By pageVerifElementLocator) throws Exception{
		
		 if(!this.isPageDisplayed(pageVerifElementLocator)) {
			  throw new Exception("The '"+pageName+"' page is not displayed.");
		 }
	}
	
	public boolean isPageDisplayed(By pageVerifElementLocator){
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(pageVerifElementLocator));

		return this.driver.findElement(pageVerifElementLocator).isDisplayed(); 
	}

}
