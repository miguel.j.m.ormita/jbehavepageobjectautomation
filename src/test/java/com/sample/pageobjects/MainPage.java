package com.sample.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class MainPage extends BasePage{	
	
	public MainPage(WebDriver lDriver) throws Exception {	
		super(lDriver);
		PageFactory.initElements(lDriver, this);
		this.validatePageDisplayed(PAGE_NAME, this.homeSliderWindowLocator);
	}
	
	private static final String PAGE_NAME = "Main";
	
	@FindBy(xpath="//a[contains(text(), 'Sign in')]")
	WebElement signInButton;
	
	MainCategoryTab catTab = new MainCategoryTab(this.driver);
	
	private static final String homeSliderWindowLocatorXpath = "//a[contains(text(),'Sign in')]";
	@FindBy(xpath=homeSliderWindowLocatorXpath)
	WebElement homeSliderWindow;
	By homeSliderWindowLocator = By.xpath(homeSliderWindowLocatorXpath);
	

	
	public AuthenticationPage clickSign() throws Exception {
		this.signInButton.click();
		
		return new AuthenticationPage(this.driver);	
	}
	




}
