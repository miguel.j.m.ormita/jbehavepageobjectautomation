package com.sample.steps;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Pending;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;

import com.sample.pageobjects.AuthenticationPage;
import com.sample.pageobjects.MainPage;
import com.sample.pageobjects.MyAccountPage;

public class MySteps extends BaseSteps{
	

	@Given("the main page is displayed")
	public void givenTheMainPageIsDisplayed() throws Exception {
		

		this.driver.get("http://automationpractice.com/index.php");
		this.driver.manage().window().maximize();
		
		new MainPage(this.driver);
	}
	
	@When("user clicks on the Women tab")
	@Pending
	public void whenUserClicksOnTheWomenTab() throws Exception {
		 //MainPage main = new MainPage(this.driver);
	}

	@Then("Tops window is displayed")
	@Pending
	public void thenTopsWindowIsDisplayed() {
	  // PENDING
	}

	@When("user clicks on T-shirts")
	@Pending
	public void whenUserClicksOnTshirts() {
		  // PENDING
		}
	
	@Then("the T-shirt items window is displayed")
	@Pending
	public void thenTheTshirtItemsWindowIsDisplayed() {
	  // PENDING
	}

	@When("user adds-to-cart the Faded Short Sleeve T-shirt")
	@Pending
	public void whenUserAddstocartTheFadedShortSleeveTshirt() {
	  // PENDING
	}

	@Then("the Product successfull added window is displayed")
	@Pending
	public void thenTheProductSuccessfullAddedWindowIsDisplayed() {
	  // PENDING
	}

	@When("user clicks Proceed to Checkout")
	@Pending
	public void whenUserClicksProceedToCheckout() {
	  // PENDING
	}

	@Then("the Shopping-Cart Summary page is displayed")
	@Pending
	public void thenTheShoppingCartSummaryPageIsDisplayed() {
	  // PENDING
	}
	
	@When("user clicks on SignIn")
	public void whenUserClicksOnSignIn() throws Exception {
		 MainPage mainPage = new MainPage(this.driver);
		 mainPage.clickSign();
	}

	@Then("the Authentication page is displayed")
	public void thenTheAuthenticationPageIsDisplayed() throws Exception {
		
		new AuthenticationPage(this.driver);
	}

	@When("user enters a valid $username")
	public void whenUserEntersAValidUsername(String username) throws Exception {
	  
		AuthenticationPage authPage = new AuthenticationPage(this.driver);
		authPage.enterUsername(username);
	}

	@When("enters a valid $password")
	public void whenEntersAValidPassword(String password) throws Exception {
		
		AuthenticationPage authPage = new AuthenticationPage(this.driver);
		authPage.enterPassword(password);
	}

	@When("clicks on Sign In")
	public void whenClicksOnSignIn() throws Exception {
		
		AuthenticationPage authPage = new AuthenticationPage(this.driver);
		authPage.clickSignIn();
	}

	@Then("the Main account page is displayed")
	public void thenTheMainAccountPageIsDisplayed() throws Exception {
	 
		new MyAccountPage(this.driver);
	}
	
}