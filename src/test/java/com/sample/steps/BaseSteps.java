package com.sample.steps;

import org.jbehave.core.annotations.AfterScenario;
import org.jbehave.core.annotations.BeforeScenario;
import org.jbehave.core.steps.Steps;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class BaseSteps extends Steps{
	
	public WebDriver driver;
	
	@BeforeScenario
	public void beforeEachScenario() {
		
		WebDriverManager.chromedriver().setup();
		this.driver = new ChromeDriver();
	}
	
	@AfterScenario
	public void afterEachScenario() {
	    this.driver.close();
	    this.driver.quit();
	}

}
